package me.jbaimoz.ubo.services;

import me.jbaimoz.ubo.dal.ClientDao;
import me.jbaimoz.ubo.dal.OwnershipDao;
import me.jbaimoz.ubo.dal.ShareDao;
import me.jbaimoz.ubo.model.Client;
import me.jbaimoz.ubo.model.NaturalPerson;
import me.jbaimoz.ubo.model.Path;
import me.jbaimoz.ubo.model.Share;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by jbaimoz on 13/10/2019.
 */
@Component
public class OwnershipService {

    @Autowired
    private OwnershipDao ownershipHome;

    @Autowired
    private ClientDao clientHome;

    @Autowired
    private ShareDao shareHome;

    public List<NaturalPerson> getUbo(long companyId) {
        return ownershipHome.findUltimateOwnerShip(companyId);
    }

    public Client getCompagnyMap(long compagnyId) {
        List<Path> paths = ownershipHome.findClientPath(compagnyId);
        Client c = clientHome.findOne(compagnyId);
        List<Share> ownersLink = shareHome.

        return null;
    }
}
