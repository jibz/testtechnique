package me.jbaimoz.ubo.controllers;

import me.jbaimoz.ubo.services.OwnershipService;
import me.jbaimoz.ubo.model.NaturalPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jbaimoz on 13/10/2019.
 */
@RestController
public class OwnershipController {

    @Autowired
    private OwnershipService ownerships;


    @RequestMapping("/ubo/{compagnyId}")
    @ResponseBody
    public List<NaturalPerson> getOwnership(@PathVariable long compagnyId) {
        return ownerships.getUbo(compagnyId);
    }

}
