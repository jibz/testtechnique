package me.jbaimoz.ubo.dal;

import me.jbaimoz.ubo.model.NaturalPerson;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by jbaimoz on 13/10/2019.
 */
public class NatPersonRowMapper implements RowMapper<NaturalPerson> {

    @Override
    public NaturalPerson mapRow(ResultSet rs, int i) throws SQLException {
        NaturalPerson np = new NaturalPerson();

        np.setId(rs.getLong("owner_id"));
        np.setUbo(rs.getDouble("percent"));
        np.setName(rs.getString("ownername"));

        return np;
    }
}
