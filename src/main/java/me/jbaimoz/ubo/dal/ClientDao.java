package me.jbaimoz.ubo.dal;

import me.jbaimoz.ubo.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * Created by jbaimoz on 13/10/2019.
 */
@Component
public class ClientDao {

    private static final String FIND_CLIENT = "select * from Client";
    private static final String DELETE_CLIENT = "delete from Client where id = ?";
    private static final String DELETE_SHARE = "delete from Share where owner_id = ? or owned_id = ?";
    private static final String ADD_CLIENT = "insert into Client(name, person) values (?, ?)";

    private JdbcTemplate jdbcGate;

    @Autowired
    public ClientDao(DataSource ds) {
        jdbcGate = new JdbcTemplate((ds));
    }

    public Client findOne(long id) {
        return jdbcGate.queryForObject(FIND_CLIENT, new Object[]{id}, Client.class);
    }

    public void delete(long id) {
        jdbcGate.update(DELETE_SHARE, new Object[]{id, id});
        jdbcGate.update(DELETE_CLIENT, new Object[]{id});
    }

    public void add(Client c) {
        String isPerson = "f";
        if (c.isPerson()) {
            isPerson = "t";
        }
        jdbcGate.update(ADD_CLIENT, new Object[]{c.getName(), isPerson});
    }
}
