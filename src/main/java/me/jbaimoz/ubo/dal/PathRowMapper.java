package me.jbaimoz.ubo.dal;

import me.jbaimoz.ubo.model.Path;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by jbaimoz on 13/10/2019.
 */
public class PathRowMapper implements RowMapper<Path> {

    @Override
    public Path mapRow(ResultSet rs, int i) throws SQLException {
        Path p = new Path();

        p.setNatPersonId(rs.getLong("owner_id"));
        p.setNatPersonName(rs.getString("ownerName"));
        Integer[] array = (Integer[])rs.getArray("path").getArray();
        long[] path = new long[array.length];
        for(int idx = 0; idx < array.length; idx++) {
            path[idx] = array[idx];
        }
        p.setPath(path);

        return p;
    }

}
