package me.jbaimoz.ubo.dal;

import me.jbaimoz.ubo.model.Client;
import me.jbaimoz.ubo.model.NaturalPerson;
import me.jbaimoz.ubo.model.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by jbaimoz on 13/10/2019.
 */
@Component
public class OwnershipDao {

    private static final String RECURSIVE_QUERY = "with recursive nodes(owned_id, ownedName, owner_id, ownerName, percent, person, path, depth) as ("
            + "select s.\"owned_id\", c1.\"cl_name\", s.\"owner_id\", c2.\"cl_name\", s.percent / 100.0, c2.person, array [s.\"owner_id\"],  1 "
            + "from Share as s, Client as c1, Client as c2 "
            + "where s.\"owned_id\" = ? and c1.\"id\" = s.\"owned_id\" and c2.\"id\" = s.\"owner_id\" "
            + "union all "
            + "select s.\"owned_id\", c1.\"cl_name\", s.\"owner_id\", c2.\"cl_name\", nd.percent * (s.percent  / 100.0), c2.person, path || s.\"owner_id\", nd.depth + 1 "
            + "from Share as s, Client as c1, Client as c2, nodes as nd "
            + "where s.\"owned_id\" = nd.owner_id and c1.\"id\" = s.\"owned_id\" and c2.\"id\" = s.\"owner_id\" "
            + ") ";

    private static final String SEPARATED_PATH = "select * from nodes where person = 't'";
    private static final String AGGREGATED_PATH = "select owner_id, ownerName, sum(percent * 100) as percent from nodes where person = 't' group by owner_id, ownerName";

    private static final String UBO_QUERY = RECURSIVE_QUERY + AGGREGATED_PATH;
    private static final String MAP_QUERY = RECURSIVE_QUERY + SEPARATED_PATH;

    private JdbcTemplate jdbcGate;

    @Autowired
    public OwnershipDao(DataSource ds) {
        jdbcGate = new JdbcTemplate(ds);
    }

    public List<NaturalPerson> findUltimateOwnerShip(long compagnyId) {
        return jdbcGate.query(UBO_QUERY, new Object[]{compagnyId}, new NatPersonRowMapper());
    }

    public List<Path> findClientPath(long compagnyId) {
        return jdbcGate.query(MAP_QUERY, new Object[]{compagnyId}, new PathRowMapper());
    }
}
