package me.jbaimoz.ubo.dal;

import me.jbaimoz.ubo.model.Client;
import me.jbaimoz.ubo.model.Share;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by jbaimoz on 13/10/2019.
 */
@Component
public class ShareDao {

    private static final String FIND_SHARE = "select * from Share where owner_id = ? and owned_id = ?";
    private static final String FIND_FOR_OWNED = "select * from Share where owned_id = ?";
    private static final String DELETE_SHARE = "delete from Share where owner_id = ? and owned_id = ?";
    private static final String ADD_SHARE = "insert into Share(owner_id, owned_id, percent) values (?, ?, ?)";
    private static final String UPD_SHARE = "update Share set percent = ? where owner_id = ? and owned_id = ?";

    private JdbcTemplate jdbcGate;

    @Autowired
    public ShareDao(DataSource ds) {
        jdbcGate = new JdbcTemplate((ds));
    }

    public Share findOne(long ownerId, long ownedId) {
        return jdbcGate.queryForObject(FIND_SHARE, new Object[]{ownerId, ownedId}, Share.class);
    }

    public void delete(Share s) {
        jdbcGate.update(DELETE_SHARE, new Object[]{s.getOwner().getId(), s.getOwned().getId()});
    }

    public void add(Share s) {
        jdbcGate.update(ADD_SHARE, new Object[]{s.getOwner().getId(), s.getOwned().getId(), s.getPercent()});
    }

    public void update(Share s) {
        jdbcGate.update(UPD_SHARE, new Object[]{s.getPercent(), s.getOwner().getId(), s.getOwned().getId()});
    }
}
