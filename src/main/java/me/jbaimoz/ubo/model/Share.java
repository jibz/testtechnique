package me.jbaimoz.ubo.model;

/**
 * Created by jbaimoz on 12/10/2019.
 */
public class Share {

    private Client owner;
    private Client owned;

    private double percent;

    public Client getOwner() {
        return owner;
    }

    public void setOwner(Client owner) {
        this.owner = owner;
    }

    public Client getOwned() {
        return owned;
    }

    public void setOwned(Client owned) {
        this.owned = owned;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }
}
