package me.jbaimoz.ubo.model;

/**
 * Created by jbaimoz on 13/10/2019.
 */
public class Path {

    private String natPersonName;
    private long natPersonId;
    private long[] path;

    public String getNatPersonName() {
        return natPersonName;
    }

    public void setNatPersonName(String natPersonName) {
        this.natPersonName = natPersonName;
    }

    public long getNatPersonId() {
        return natPersonId;
    }

    public void setNatPersonId(long natPersonId) {
        this.natPersonId = natPersonId;
    }

    public long[] getPath() {
        return path;
    }

    public void setPath(long[] path) {
        this.path = path;
    }
}
