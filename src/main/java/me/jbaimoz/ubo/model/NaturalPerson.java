package me.jbaimoz.ubo.model;

/**
 * Created by jbaimoz on 13/10/2019.
 */
public class NaturalPerson extends Client {

    private double ubo;

    public NaturalPerson() {
        super.setPerson(true);
    }

    public double getUbo() {
        return ubo;
    }

    public void setUbo(double ubo) {
        this.ubo = ubo;
    }

    @Override
    public void setPerson(boolean person) {
        super.setPerson(true);
    }
}
