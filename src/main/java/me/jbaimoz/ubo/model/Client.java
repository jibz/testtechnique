package me.jbaimoz.ubo.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jbaimoz on 12/10/2019.
 */
public class Client {

    private long id;
    private String name;
    private boolean person;

    private List<Share> ownedBy;

    public Client() {
        ownedBy = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Share> getOwnedBy() {
        return ownedBy;
    }

    public void setOwnedBy(List<Share> ownedBy) {
        this.ownedBy = ownedBy;
    }

    public void addShare(Share share) {
        ownedBy.add(share);
    }

    public boolean isPerson() {
        return person;
    }

    public void setPerson(boolean person) {
        this.person = person;
    }
}
