package me.jbaimoz.ubo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * Created by jbaimoz on 12/10/2019.
 */
@Configuration
@ComponentScan("me.jbaimoz.ubo")
public class AppConfig {

    @Bean
    public DataSource pgDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/owningmap");
        dataSource.setUsername("jbaimoz");

        return dataSource;
    }
}
