
-- Find all path and get independent share holding for leaf node
with recursive nodes(owned_id, ownedName, owner_id, ownerName, percent, person, path, depth) as (
  select
    s."owned_id",
    c1."cl_name",
    s."owner_id",
    c2."cl_name",
    s.percent / 100.0,
    c2.person,
    array [s."owner_id"],
    1
  from Share as s, Client as c1, Client as c2
  where s."owned_id" = 8
        and c1."id" = s."owned_id" and c2."id" = s."owner_id"
  union all
  select
    s."owned_id",
    c1."cl_name",
    s."owner_id",
    c2."cl_name",
    nd.percent * (s.percent / 100.0),
    c2.person,
    path || s."owner_id",
    nd.depth + 1
  from Share as s, Client as c1, Client as c2, nodes as nd
  where s."owned_id" = nd.owner_id
        and c1."id" = s."owned_id" and c2."id" = s."owner_id"
)
select * from nodes where person = 't';

-- Find aggregated share holding
with recursive nodes(owned_id, ownedName, owner_id, ownerName, percent, person, path, depth) as (
  select
    s."owned_id",
    c1."cl_name",
    s."owner_id",
    c2."cl_name",
    s.percent / 100.0,
    c2.person,
    array [s."owner_id"],
    1
  from Share as s, Client as c1, Client as c2
  where s."owned_id" = 8
        and c1."id" = s."owned_id" and c2."id" = s."owner_id"
  union all
  select
    s."owned_id",
    c1."cl_name",
    s."owner_id",
    c2."cl_name",
    nd.percent * (s.percent / 100.0),
    c2.person,
    path || s."owner_id",
    nd.depth + 1
  from Share as s, Client as c1, Client as c2, nodes as nd
  where s."owned_id" = nd.owner_id
        and c1."id" = s."owned_id" and c2."id" = s."owner_id"
)
select owner_id, ownerName, sum(percent * 100) as percent from nodes where person = 't' group by owner_id, ownerName;