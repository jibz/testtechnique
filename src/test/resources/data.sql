delete from Share;
delete from Client;

insert into Client(id, cl_name, person) values (1, 'John', 't'), (2, 'Mary', 't'), (3, 'Sunny Island Offshore LLC', 'f'), (4, 'Smart investment Inc', 'f'),
  (5, 'Family Co LLC', 'f'), (6, 'ABC Holding LLC', 'f'), (7, 'XYZ Investments Inc', 'f'), (8, 'Client A', 'f');

insert into Share(owner_id, owned_id, percent) values (1, 3, 100.0), (1, 4, 50.0), (1, 5, 10.0), (2, 5, 90.0), (3, 6, 50.0),
  (4, 6, 50.0), (4, 8, 10.0), (5, 7, 60.0), (6, 8, 50.0), (7, 8, 40.0);