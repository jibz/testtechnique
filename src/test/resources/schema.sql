-- drop database owningmap;
-- create database owningmap;

create table if not exists Client (
  id serial primary key,
  cl_name varchar(64),
  person bool default 'f'
);

create table if not exists Share (
  owner_id integer references Client(id),
  owned_id integer references CLient(id),
  percent numeric(5, 2) not null,
  primary key(owner_id, owned_id)
);