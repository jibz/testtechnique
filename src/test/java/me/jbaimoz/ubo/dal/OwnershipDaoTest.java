package me.jbaimoz.ubo.dal;

import me.jbaimoz.ubo.AppConfig;
import me.jbaimoz.ubo.model.NaturalPerson;
import me.jbaimoz.ubo.model.Path;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

/**
 * Created by jbaimoz on 13/10/2019.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class OwnershipDaoTest {

    private static final long COMPAGNY_ID = 8;

    @Autowired
    private OwnershipDao testDao;

    public OwnershipDaoTest() {

    }

    @Test
    @Sql(scripts = {"/schema.sql", "/data.sql"}, config = @SqlConfig(dataSource = "pgDataSource"))
    public void whenCheckForCompagny8_thenFinds2Persons() {
        List<NaturalPerson> persons = testDao.findUltimateOwnerShip(COMPAGNY_ID);
        Assert.assertEquals(2, persons.size());
        NaturalPerson mary = persons.get(0);
        NaturalPerson john = persons.get(1);

        Assert.assertEquals(2, mary.getId());
        Assert.assertEquals("Mary", mary.getName());
        Assert.assertEquals(21.6, mary.getUbo(), 0.0);

        Assert.assertEquals(1, john.getId());
        Assert.assertEquals("John", john.getName());
        Assert.assertEquals(44.9, john.getUbo(), 0.0);
    }

    @Test
    @Sql(scripts = {"/schema.sql", "/data.sql"}, config = @SqlConfig(dataSource = "pgDataSource"))
    public void whenAskForCompagny8Map_thenShouldFindAllOwingPath() {
        List<Path> paths = testDao.findClientPath(8);

        Assert.assertEquals(5, paths.size());
        checkPath(paths.get(0), "John", 1, new long[]{4, 1});
        checkPath(paths.get(1), "John", 1, new long[]{6, 3, 1});
        checkPath(paths.get(2), "John", 1, new long[]{6, 4, 1});
        checkPath(paths.get(3), "John", 1, new long[]{7, 5, 1});
        checkPath(paths.get(4), "Mary", 2, new long[]{7, 5, 2});

    }

    private void checkPath(Path p, String name, long id, long[] path) {
        Assert.assertEquals(id, p.getNatPersonId());
        Assert.assertEquals(name, p.getNatPersonName());
        Assert.assertArrayEquals(path, p.getPath());
    }
}
