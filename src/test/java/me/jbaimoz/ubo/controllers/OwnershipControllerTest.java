package me.jbaimoz.ubo.controllers;

import me.jbaimoz.ubo.AppConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by jbaimoz on 13/10/2019.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebMvcTest(OwnershipController.class)
public class OwnershipControllerTest {

    static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );

    @Autowired
    private MockMvc mvc;


    @Test
    public void whenQueryingForBeneficialOwnerOfCompagny8_thenGet2NaturalPersons() throws Exception {
        mvc.perform(get("/ubo/8").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(2)))
                .andExpect(jsonPath("$[0].name", is("Mary")))
                .andExpect(jsonPath("$[0].ubo", is(21.6)))
                .andExpect(jsonPath("$[1].id", is(1)))
                .andExpect(jsonPath("$[1].name", is("John")))
                .andExpect(jsonPath("$[1].ubo", is(44.9)));

    }

    @Test
    public void whenQueryingForOwnerMapOfCompany8_thenGetAMapOf8Node() throws Exception {
        mvc.perform(get("/map/8").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
